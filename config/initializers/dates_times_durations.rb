
# def duration_of_interval_in_words(interval)
#   hours, minutes, seconds = interval.split(':').map(&:to_i)

#   [].tap do |parts|
#     parts << "#{hours} hour".pluralize(hours)       unless hours.zero?
#     parts << "#{minutes} minute".pluralize(minutes) unless minutes.zero?
#     parts << "#{seconds} hour".pluralize(seconds)   unless seconds.zero?
#   end.join(', ')
# end

class Numeric
  # def duration
  #   secs  = self.to_int
  #   mins  = secs / 60
  #   hours = mins / 60
  #   days  = hours / 24

  #   if days > 0
  #     "#{days} days and #{hours % 24} hours"
  #   elsif hours > 0
  #     "#{hours} hours and #{mins % 60} minutes"
  #   elsif mins > 0
  #     "#{mins} minutes and #{secs % 60} seconds"
  #   elsif secs >= 0
  #     "#{secs} seconds"
  #   end
  # end

  def date_duration
    days  = self.to_int
    weeks  = days / 7
    months  = days / 30

    if months > 0
      mweeks = (weeks % 4)
      "#{months} " +
      (months > 1 ? 'månader' : 'månad') +
      (mweeks > 0 ? ", #{mweeks} #{mweeks > 1 ? 'veckor' : 'vecka'}" : '')

    elsif weeks > 0
      wdays = days % 7
      "#{weeks} " +
      (weeks > 1 ? 'veckor' : 'vecka') +
      (wdays > 0 ? ", #{wdays} #{wdays > 1 ? 'dagar' : 'dag'}" : '')

    elsif days >= 0
      "#{days} " + (days > 1 ? 'dagar' : 'dag')
    end
  end
end
