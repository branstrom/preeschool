ActiveRecord::QueryMethods::WhereChain.class_eval do
  def or(*scopes)
    scopes_where_values = []
    scopes_bind_values  = []
    scopes.each do |scope|
      case scope
      when ActiveRecord::Relation
        scopes_where_values << scope.where_values.reduce(:and)
        scopes_bind_values += scope.bind_values
      when Hash
        temp_scope = @scope.model.where(scope)
        scopes_where_values << temp_scope.where_values.reduce(:and)
        scopes_bind_values  += temp_scope.bind_values
      end
    end
    scopes_where_values = scopes_where_values.inject(:or)
    @scope.where_values += [scopes_where_values]
    @scope.bind_values  += scopes_bind_values
    @scope
  end
end

class ActiveRecord::Base

  def history
    raise "model needs to have paper_trail" unless self.versions
    changesets = []
    all_versions = self.versions.map(&:reify).compact.concat([self])
    all_versions.each_with_index do |version, index|
      if index > 0 && older_version = all_versions[index-1]
        changesets << older_version.attributes
          .deep_diff(version.attributes)
          .except("created_at", "updated_at")
          .merge("updated_at": version.attributes["updated_at"])
          .with_indifferent_access
      else
        changesets << version.slice("created_at", "updated_at")
          .with_indifferent_access
      end
    end
    changesets
  end

end