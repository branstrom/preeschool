Premailer::Rails.config.merge!(
  include_link_tags: false, # Otherwise the <link> tag will be parsed twice, once by Premailer::Rails and once by Premailer
  remove_ids: true,
  remove_classes: true
)