module Adroit
  class AgeWhen < Age

    def initialize(at_date)
      if at_date.acts_like_date?
        @@now = at_date
      end
    end

  end
end

# TODO: method for getting current time? @@now reads when class is inited, doesn't it! :/

class Date
  def find_age
    Adroit::AgeWhen.new(Date.today).find_year self
  end

  def find_age_by_end_of_year
    now = Date.today
    end_of_year = now.change(month: 12, day: 31)
    Adroit::AgeWhen.new(end_of_year).find_year self
  end

  def find_age_on(date)
    Adroit::AgeWhen.new(date).find_year self
  end

  def find_age_with_month year_cust = nil, month_cust = nil
    Adroit::Age.new.find_with_month self, year_cust, month_cust
  end
end
