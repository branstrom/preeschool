Preeschool::Application.routes.draw do

  # resources :kids
  # resources :families
  resources :invoices
  resources :registrations, only: [:new, :edit, :create, :update]
  #root to: 'kids#index'

  mount RailsEmailPreview::Engine, at: 'emails'


  namespace :admin do
    resources :messages
    resources :kids
    resources :families do
      get 'leads', action: :leads, on: :collection
    end
    resources :invoices do
      post 'notice', action: :notice, on: :member
      post 'reminder', action: :reminder, on: :member
      post 'alert', action: :alert, on: :member
      post 'mark_as_payed', action: :mark_as_payed, on: :member
      post 'replace', action: :replace, on: :member
      post 'cancel', action: :cancel, on: :member

      get 'check_payments', action: :check_payments, on: :collection
      post 'check_payments', action: :check_payments, on: :collection
    end
    resources :finances
  end

  comfy_route :gallery_admin, path: '/admin'
  comfy_route :cms_admin, path: '/admin'

  comfy_route :blog_admin, path: '/admin'
  comfy_route :blog, path: '/blogg'

  # TODO: should we have a hardcoded 404 or can we use a page?
  # get '/404', action: 'invoices#index', as: :error, controller: 'invoices'
  get '/bankid', to: redirect('bankid:///?redirect=http://forskolan-solsidan.se/admin/invoices?from_bankid=true'), as: :bankid

  # Make sure this routeset is defined last
  comfy_route :cms, path: '/', sitemap: false


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', as: :purchase
  # This route can be invoked with purchase_url(id: product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
