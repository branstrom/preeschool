# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron
#
# Learn more: http://github.com/javan/whenever

set :output, {:error => 'error.log', :standard => 'cron.log'}
env :PATH, ENV['PATH']

environment = (ENV['RAILS_ENV'] || 'production').to_sym
env :RAILS_ENV, environment
set :environment, environment

every 1.month do
  runner "Invoice.create_for_families"
end

# TODO: check timezone diff
#
every :day, at: '17:50' do
  runner "Invoice.send_notices_and_reminders"
end

every 3.days, at: '10:30' do
  runner "Payment.email_and_try_to_fetch_new!"
end

every 6.hours do
  runner "puts 'Ping. ' + Time.now.to_s"
end
