class AddTagsToComfyBlog < ActiveRecord::Migration
  def change
  	create_table :blog_post_tags do |t|
      t.string :name, null: false
    end

    create_table :blog_post_taggings do |t|
      t.integer :blog_post_id, null: false
      t.integer :tag_id, null: false
    end
  end
end
