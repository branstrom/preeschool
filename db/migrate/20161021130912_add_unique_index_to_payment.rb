class AddUniqueIndexToPayment < ActiveRecord::Migration
  def change
    add_index :payments, [:amount, :from, :reference, :given_on], unique: true
  end
end
