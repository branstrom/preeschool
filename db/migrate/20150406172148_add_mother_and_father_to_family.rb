class AddMotherAndFatherToFamily < ActiveRecord::Migration
  def change
    change_table :families do |t|
      t.rename :full_name, :mother_name
      t.rename :email, :mother_email
      t.rename :phone, :mother_phone
      
      t.string :father_name
      t.string :father_email
      t.string :father_phone
    end
  end
end
