class AddLeadToFamily < ActiveRecord::Migration
  def change
    add_column :families, :is_lead, :boolean
    add_column :families, :lead_comments, :text
  end
end
