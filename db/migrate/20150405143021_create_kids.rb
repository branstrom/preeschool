class CreateKids < ActiveRecord::Migration

  def change
    create_table :kids do |t|
      t.string :full_name
      t.string :ssn
      t.boolean :part_time
      t.date :start_date
      t.date :end_date
      t.text :comment
      t.timestamps
    end
  end

end