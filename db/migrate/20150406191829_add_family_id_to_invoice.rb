class AddFamilyIdToInvoice < ActiveRecord::Migration
  def change
    add_reference :invoices, :family, index: true, foreign_key: true
  end
end
