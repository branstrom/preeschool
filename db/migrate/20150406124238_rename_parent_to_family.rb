class RenameParentToFamily < ActiveRecord::Migration
  def change
  	rename_table :parents, :families
  end
end
