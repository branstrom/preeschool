class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.decimal :amount, null: false
      t.string :from
      t.string :reference, null: false
      t.string :avi_id, index: { unique: true }
      t.references :invoice, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
