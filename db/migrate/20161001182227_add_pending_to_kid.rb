class AddPendingToKid < ActiveRecord::Migration
  def change
    add_column :kids, :pending, :boolean
  end
end
