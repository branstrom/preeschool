class AddGivenOnToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :given_on, :date
  end
end
