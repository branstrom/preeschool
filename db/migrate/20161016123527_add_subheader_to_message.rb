class AddSubheaderToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :subheader, :string
  end
end
