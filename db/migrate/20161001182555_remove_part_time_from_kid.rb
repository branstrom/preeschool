class RemovePartTimeFromKid < ActiveRecord::Migration
  def change
  	remove_column(:kids, :part_time)
  end
end
