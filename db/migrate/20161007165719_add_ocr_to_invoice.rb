class AddOcrToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :ocr, :string
  end
end
