class AddSuccessToPaymentCheck < ActiveRecord::Migration
  def change
    add_column :payment_checks, :success, :boolean, null: false, default: false
  end
end
