class AddPayedOnToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :payed_on, :date
  end
end
