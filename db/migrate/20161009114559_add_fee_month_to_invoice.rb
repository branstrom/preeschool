class AddFeeMonthToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :fee_month, :date
  end
end
