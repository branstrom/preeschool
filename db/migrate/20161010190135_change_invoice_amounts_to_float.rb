class ChangeInvoiceAmountsToFloat < ActiveRecord::Migration
  def change
    change_column :invoices, :amount, :float
    change_column :invoices, :amount_due, :float
  end
end
