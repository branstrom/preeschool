class AddTimestampsToMessage < ActiveRecord::Migration
  def change
	add_timestamps(:messages, null: false)
  end
end
