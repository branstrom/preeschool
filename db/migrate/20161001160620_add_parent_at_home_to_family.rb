class AddParentAtHomeToFamily < ActiveRecord::Migration
  def change
    add_column :families, :parent_at_home, :boolean
  end
end
