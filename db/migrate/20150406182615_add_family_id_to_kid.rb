class AddFamilyIdToKid < ActiveRecord::Migration
  def change
    add_reference :kids, :family, index: true, foreign_key: true
  end
end
