class AddLastRemindedToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :last_reminded, :date
  end
end
