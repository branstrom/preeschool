class RemoveNullConstraintOnPayment < ActiveRecord::Migration
  def change
  	change_column_null(:payments, :reference, true)
  end
end
