class AddCanceledToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :canceled, :boolean
  end
end
