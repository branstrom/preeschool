class RemoveIndexOnAviIdFromPayment < ActiveRecord::Migration
  def change
	remove_index :payments, :avi_id
  end
end
