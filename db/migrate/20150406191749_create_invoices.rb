class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.date :invoice_date
      t.date :due_date
      t.integer :amount
      t.integer :amount_due
    end
  end
end
