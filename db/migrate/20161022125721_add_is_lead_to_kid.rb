class AddIsLeadToKid < ActiveRecord::Migration
  def change
    add_column :kids, :is_lead, :boolean
  end
end
