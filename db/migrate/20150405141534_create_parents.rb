class CreateParents < ActiveRecord::Migration

  def change
    create_table :parents do |t|
      t.string :full_name
      t.string :email
      t.string :phone
      t.integer :income
      t.text :comment
      t.timestamps
    end
  end

end