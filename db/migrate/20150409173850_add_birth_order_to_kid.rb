class AddBirthOrderToKid < ActiveRecord::Migration
  def change
    add_column :kids, :birth_order, :integer
  end
end
