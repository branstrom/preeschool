class AddLastAlertToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :last_alert, :date
  end
end
