class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :family
      t.text :text
    end
  end
end
