# Preeschool

class Kid
	attrs
		pnr
		start: whenever
		end: Aug 1, year turning 6
		siblings: Kids
		parents: Parents
		part_time: bool

		big: > 3 years of age
		public_school: >= 3 years of age
		fee:
			3% kid #1
			2% kid #2
			1% kid #3
			max income 42000 == 1260 3% fee
end

class Parent
	email
	address
end

# TODO: view for Parent
# TODO: inmatning av lönenivåer för föräldrar och maxtaxa

# TODO: lusläs kommunens regler för sänkt avgift för fri förskola

# TOOD: maila ut till föräldrar och ange avgift
# TODO: track when payed - need id from payment systems to check (or code for each kid+month+year?)

# TODO: generera faktura
# TODO: undersök inbetalningsmetoder: PayPal, PayNova, Swish, bankgiro
# TODO: generera e-faktura ..?
# TODO: avvakta e-bokföring?
# TODO: undersök autogiro ..?


# TODO: löneuträkningar för personal ..? (beror inte av denna data dock - skulle isf bli separat modul)


