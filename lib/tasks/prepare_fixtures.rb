namespace :test do
  task :prepare do
    ENV['FROM'] = 'solsidan'
    ENV['TO']   = 'solsidan'

    Comfy::Cms::Site.create!(:identifier => 'solsidan', :hostname => 'localhost')
    # TODO: import gallery as well
    Rake::Task['comfortable_mexican_sofa:fixtures:import'].invoke
  end
end