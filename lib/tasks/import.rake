# encoding: utf-8
require "google_drive"
require 'awesome_print'

namespace :import do
  desc "Prints Country.all in a seeds.rb way."
  task :google_sheet => :environment do
    # if Kid.any? || Family.any? || Invoice.any?
    #   raise 'Database must be cleared of any Kid, Family, Invoice, etc'
    # end

    # https://github.com/gimite/google-drive-ruby/blob/master/doc/authorization.md
    session = GoogleDrive::Session.from_service_account_key("lib/tasks/config_service_account.json")

    spreadsheet = session.spreadsheet_by_key("15mn0mW2NyGWwLjXB4yFAH2Iu1j_B-TAckjkqqlTfcZY")
    ws = spreadsheet.worksheet_by_title('2021')

    header_1 = ws.rows[0]
    header_2 = ws.rows[1]

    make_kid_from_row(ws.rows[2]) # Oskar Forsberg
    make_kid_from_row(ws.rows[25]) # Livia Widinghof

    # ws.rows.each do |row|
    #   ap Kid.new(
    #     full_name: row[]
    #   )
    # end
  end

  def make_kid_from_row(row)
    kid = Kid.new(
      full_name: row[0],
      ssn: row[2].gsub(/\D/, ''),
    )

    raise 'no date of birth' unless kid.date_of_birth
    kid.start_date = kid.date_of_birth + 1.year
    kid.save!

    # puts 'Just tried to create Kid:'
    # ap kid
    # puts 'Kid valid? ', kid.valid?
    # puts kid.errors.full_messages
    kid
  end
end

