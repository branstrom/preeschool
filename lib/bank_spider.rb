#coding: utf-8
require 'selenium-webdriver'
require 'awesome_print'
require 'vessel'

class BankSpider < Vessel::Cargo
  # delay 2 #, timeout: 3

  domain 'online.swedbank.se'
  start_urls 'https://online.swedbank.se/app/ib/logga-in'

  def parse
    sleep 2
    userid_field = at_css("input[type=text]")
    ap userid_field
    raise 'field not found' unless userid_field
    userid_field.focus.type(ENV['admin_ssn'])

    login_button = at_css("[type=submit]")
    raise 'field not found' unless login_button
    # login_button.focus.click

    message = at_css("span[slot=message]")&.text
    login_message = at_css(".mobile-bank-id__qr-code--instruction-list")&.text

    # url = absolute_url(next_page[:href])
    # yield request(url: url, handler: :parse)
  end

end

# def self.login_and_fetch_payments(browser = nil)
#   result = nil

#   bank_page = SwedbankPage.new(browser)
#   result = bank_page.login_and_process_bankgirot(ENV['admin_ssn'])
# ensure
#   browser.quit if browser
#   result
# end

class SwedbankPage
  include PageObject

  default_element_wait = 4

  page_url 'https://online.swedbank.se/app/ib/logga-in'

  text_field(:username, type: 'text')
  # select_list(:login_method, required: 'required')
  button(:login, type: 'submit')

  def login_and_process_bankgirot(userid)
    login_with(userid)
    jump_to_bankgirot
    BankgiroPage.new(browser).process
  # Remember to disable rescue before deploying!
  # rescue => e
  #   puts e
  #   debugger
  #   browser.html
  end

  def login_with(userid)
    goto
    # uri = 'file://' + File.expand_path('./html/bank_login_form_201708.html', __dir__)
    # navigate_to uri

    # send_keys([:command, 'h'])
    wait_until(4, 'Login unavailable') do
      browser.text.include? 'hur du vill logga in'
    end

    # debugger
    # browser.text_field(id: "inputElementId--0").set(userid)
    # browser.text_fields.first.locate.set(userid)
    username_element.set(userid)

    # browser.execute_script("document.querySelector('input[type=text]').click();")
    # browser.execute_script("document.querySelector('input[type=text]').value = '#{userid}';")
    # browser.execute_script("document.querySelector('input[type=text]').click();")

    # browser.execute_script("document.querySelector('[type=submit]').click();")

    login

    # TODO handle 'Påloggningen är redan påbörjad.'

    wait_until(4, 'Mobile BankID login failed') do
      browser.text.include?('Nu kontaktas din mobila enhet') ||
        browser.text.include?('Väntar på svar från Mobilt BankID')
    end

    # if browser.text.include? 'Påloggning är redan påbörjad'
    #

    # puts "================================"
    # puts "AFTER LOGIN ==================="
    # puts "================================"
    # puts browser.html

    wait_until(30, 'Unsuccessful login attempt - could not locate company name') do
      company_admin_choice_element.present? #|| browser.text.include?('Egna konton')
    end
    true
    # wait_until(40, 'Unsuccessful login attempt') do
    #   company_admin_link_element.present? #|| browser.text.include?('Egna konton')
    # end
  end

  # div(:menu_item, id: 'toplevel_Tillvalstjenster')
  span(:bankgiro_menu_item, class: 'menu-item-label', text: /Bankgirotjänster/)
  # span(:bg_menu_item, class: 'menu-item-label', text: /Koder och lösenord/)
  button(:bankgiro_button, class: 'knapp', name: 'visa_ankommande')
  span(:company_admin_choice, text: /Förskolan Solsidan AB/)

  def jump_to_bankgirot
    # goes to https://internetbank.swedbank.se/bviforetagplus/PortalBvIForetagPlus?TDEApplName=TDEApplBankgiro&_flow_id_=BANKGIRO_VISA_BANKGIRONUMMER_PRECLIENT&_new_flow_=true&menuId=EkonomiskOversikt_Bankgirotjenster
    # wait_until(5, 'Could not locate company link') do
    #   company_admin_link_element.visible?
    # end
    company_admin_choice_element.click
    wait_until(5, 'Swedbank page did not load correctly') do
      browser.h2(text: 'Startsida').present?
    end
    # browser.execute_script("location.href = objItems.EkonomiskOversikt_Bankgirotjenster.action")
    browser.execute_script("showMainMenu('ekonomiskoversikt', document.getElementById('toplevel_ekonomiskoversikt'))")

    # browser.execute_script("showMainMenu('Tillvalstjenster', document.getElementById('toplevel_Tillvalstjenster'))")
    wait_until(1, 'Menu did not open') do
      # browser.div(id: 'mc_Tillvalstjenster').visible?
      browser.div(id: 'mc_ekonomiskoversikt').visible?
    end
    # wait_until(1, 'No Bankgirotjänster menu item present') do
    #   bg_menu_item_element.visible?
    # end

    bankgiro_menu_item_element.when_present.click
    bankgiro_button_element.when_present.click
  end
end

#<Watir::Browser:0x..fe5ce0c5d3987214c url="https://www.insup.bgonline.se/ELINWeb/Templates/ManyDays.aspx?AllDays=true&Search=AllDays&Digest=HHfeLbWzunOlGd2UROTAqQ" title="Insättningsuppgift via internet">
class BankgiroPage
  include PageObject

  expected_title('Insättningsuppgift via internet')
  button(:bg_search_button, class: 'SearchBtn', type: 'submit')

  def test_process
    uri = 'file://' + File.expand_path('./html/bg_whole_index.html', __dir__)
    navigate_to uri
    process_payments(Date.new(2016,10,7))
  # rescue => exception
  #   debugger
  #   browser.html
  end

  def process
    has_expected_title?
    browser.windows.last.use
    result = process_payments
  rescue => exception
    handle(exception)
  ensure
    result
  end

  def handle(exception)
    puts "Problem vid inläsning från Bankgirot: #{exception}"
    debugger
    Rails.logger.error "Problem vid inläsning från Bankgirot: #{exception}"
    puts browser.html
    Rails.logger.info browser.html
  end

  table(:bg_payments_table, class: 'GridView')
  table(:bg_payment_table, class: 'GridView')

  def find_payment_bundles
    bg_search_button_element.when_present.click
    # TODO: scope to <tbody>
    result = bg_payments_table_element.when_present.row_elements
  rescue => exception
    handle(exception)
  ensure
    result
  end


  def process_payments(earliest_date = 1.month.ago)
    @payments = {}

    # collect dates and hrefs first, they won't be available later
    date_links = find_payment_bundles.map do |row|
      next if row.class_name == 'Gridview_header'
      cell = row.cell_element(class: 'Date') #.when_present
      raise 'date cell missing' unless cell.present?
      { date: cell.text, href: cell.link_elements[0].href }
    end.compact

    date_links.map do |bundle|
      date = Date.parse(bundle[:date]) # => "2016-10-18"
      # next if date != Date.new(2016,10,14) # TESTING
      next if date < earliest_date
      @payments[date] = []
      process_payment_page(date, bundle[:href])
    end

    # ap @payments

    # puts "ALL PAYMENTS COLLECTED FROM BANKGIROT:"
    Rails.logger.info "ALL PAYMENTS COLLECTED FROM BANKGIROT:"
    # puts @payments
    Rails.logger.info @payments
    @payments
  rescue => exception
    handle(exception)
  ensure
    @payments
  end


  body(:body, tag_name: 'body')

  # TODO make sure href value is safely used
  def process_payment_page(date, href)
    begin
      # puts "Jumping to payment table for #{date.to_s} #{href.to_s}"
      browser.execute_script("window.location.href = \'#{href}\';")
      wait_until(10, "No body element visiblee") { body_element.visible? }
      logged_out = body_element.text.include? 'utloggad'
      wait_until(10, "No BankGiro table element visible") { bg_payment_table_element.visible? }

      # puts "Trying to read payment table cells: amount,from,reference,avi_id, here's the current HTML:"
      # puts browser.html

    rescue => exception
      handle(exception)
      browser.back
      return
    end

    if logged_out || !bg_payment_table_element.present?
      puts 'Logged out or not seeing the payment table'
      Rails.logger.error 'Logged out or not seeing the payment table'
      browser.back
      return
    end

    # FIXME: why does a payment of 4 oct wind up in 3 oct bundle?

    payment_rows = bg_payment_table_element.when_present.row_elements

    payment_rows.each do |row|
      @payments[date] << {
        amount: (row.when_present.cell_element(class: 'AmountColumn').when_present.text rescue nil),
        from: (row.when_present.cell_elements[1].when_present.text rescue nil),
        reference: (row.when_present.cell_elements[2].when_present.text rescue nil),
        avi_id: (row.when_present.cell_elements[3].when_present.text rescue nil)
      }
    end
  rescue => exception
    handle(exception)
  end

end

