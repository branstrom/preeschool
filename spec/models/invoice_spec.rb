# encoding: utf-8
require 'rails_helper'
# Time is April 1, 2015, 10.05 AM

RSpec.describe Invoice, type: :model do
  before(:all) do
    jump(2015, 4, 2)
    make_families
  end

  after(:all) do
    clear_tables
  end

  it "should be created and be the most recent invoice" do
    expect(@invoice).not_to be_nil
    expect(@invoice).to eq(@family.invoices.newest)
  end

  it "should have an ocr number" do
    expect(@invoice.ocr).to eq("805550860")
  end

  it "should have the same ocr number every time" do
    @invoice.update_attributes(ocr: nil)
    expect(@invoice.ocr).to eq("805550860")
  end

  it "should have an amount" do
    expect(@invoice.amount).to eq(@family.total_fee)
  end

  it "should have an amount_due" do
    expect(@invoice.amount_due).to eq(@invoice.amount)
  end

  it "needs a family" do
    invoice = Invoice.create
    expect(invoice.persisted?).to be false
  end

  it "should have the correct invoice date on 13th day of month" do
    expect(@invoice.invoice_date).to eq(Date.new(2015,4,2))

    jump(2015, 4, 13)

    invoice = @family.invoices.create
    expect(invoice.invoice_date).to eq(Date.new(2015,4,13))
  end

  it "should have the correct invoice date on 1st day of month" do
    jump(2015, 4, 1)

    invoice = @family.invoices.create
    expect(invoice.invoice_date).to eq(Date.new(2015,4,1))
  end

  it "should have the correct due date" do
    jump(2015, 4, 1)

    invoice = @family.invoices.create
    expect(invoice.due_date).to eq(Date.new(2015,4,26))
  end

  it "should have the correct fee_month" do
    jump(2015, 4, 1)

    invoice = @family.invoices.create
    expect(invoice.fee_month).to eq(Date.new(2015,4,1))
  end

  it "should have the correct fee_month when created later" do
    jump(2015, 4, 7)

    invoice = @family.invoices.create
    expect(invoice.fee_month).to eq(Date.new(2015,4,1))
  end

  it "should be able to create a 2nd invoice next month" do
    jump(2015, 5, 1)

    invoice = @family.invoices.create
    expect(invoice.invoice_date).to eq(Date.new(2015,5,1))
    expect(invoice).to eq(@family.invoices.newest)
  end

  # used to be that it shouldn't
  it "should create invoice for summer months" do
    # normal create
    jump(2015, 7, 2)
    invoice = @family.invoices.create
    expect(invoice.persisted?).to be true

    # create with fee_month in the past
    jump(2016, 8, 2)
    invoice = @family.invoices.create(fee_month: Date.new(2016,7,1))
    expect(invoice.persisted?).to be true

    jump(2015, 4, 1)
  end

  it "should generate invoices only for families with a fee" do
    expect(@family2.total_fee).to eq 0
    expect(@family3.total_fee).to eq 1311

    jump(2015, 10, 3)
    Invoice.create_for_families
    expect(@family2.invoices.newest).to eq(nil)
    expect(@family.invoices.newest.invoice_date).to eq(Date.new(2015,10,3))
    expect(@family.invoices.newest.fee_month).to eq(Date.new(2015,10,1))
    expect(@family3.invoices.newest.invoice_date).to eq(Date.new(2015,10,3))
    expect(@family3.invoices.newest.fee_month).to eq(Date.new(2015,10,1))
  end

  it "should be possible to generate invoices for fee_month in the past" do
    jump(2015, 4, 1)
    Invoice.create_for_families(fee_month: Date.new(2015,1,1))

    expect(@family.invoices.oldest.fee_month).to eq(Date.new(2015,1,1))
    expect(@family3.invoices.oldest.fee_month).to eq(Date.new(2015,1,1))
    # expect(@family3.invoices.first.invoice_date).to eq(Date.new(2015,10,3))
  end

  it "should tell if overdue" do
    expect(@invoice.fully_payed?).to be false
    jump(2015, 4, 20)
    expect(@invoice.overdue?).to be false
    expect(@invoice.due_date).to eq Date.new(2015,4,27)
    jump(2015, 4, 29)
  end

  it "should have an overdue scope" do
    jump(2015, 4, 20)
    expect(Invoice.overdue).to eq([])
    jump(2015, 4, 29)
    expect(Invoice.overdue).to eq([@invoice])
  end

  it "should tell how long overdue" do
    jump(2015, 4, 20)
    expect(@invoice.overdue_days).to be(0)
    jump(2015, 4, 30)
    expect(@invoice.overdue_days).to be(3)
  end

  it "should have a readable overdue_duration" do
    jump(2015, 4, 20)
    expect(@invoice.overdue_duration).to eq('')
    jump(2015, 4, 30)
    expect(@invoice.overdue_duration).to eq('3 dagar')
    jump(2015, 5, 4)
    expect(@invoice.overdue_duration).to eq('1 vecka')
    jump(2015, 5, 11)
    expect(@invoice.overdue_duration).to eq('2 veckor')
    jump(2015, 5, 12)
    expect(@invoice.overdue_duration).to eq('2 veckor, 1 dag')
    jump(2015, 5, 27)
    expect(@invoice.overdue_duration).to eq('1 månad')
    jump(2015, 6, 2)
    expect(@invoice.overdue_duration).to eq('1 månad, 1 vecka')
    jump(2015, 6, 8)
    expect(@invoice.overdue_duration).to eq('1 månad, 2 veckor')
    jump(2015, 7, 5)
    expect(@invoice.overdue_duration).to eq('2 månader, 1 vecka')
  end

  it "should tell if not_fully_payed" do
    jump(2015, 4, 1)

    expect(@invoice.not_fully_payed?).to be true
    invoice = @family3.invoices.create
    invoice.pay!(invoice.amount-1)
    expect(invoice.not_fully_payed?).to eq(true)
    invoice.pay!(1)
    expect(invoice.not_fully_payed?).to eq(false)
  end

  it "should have scope not_fully_payed" do
    expect(Invoice.not_fully_payed).to eq([@invoice])
    invoice = @family3.invoices.create
    expect(Invoice.not_fully_payed).to eq([@invoice, invoice])
    invoice.pay!(invoice.amount-1)
    expect(Invoice.not_fully_payed).to eq([@invoice, invoice])
    invoice.pay!(1)
    expect(Invoice.not_fully_payed).to eq([@invoice])
  end

  it "should be possible to pay the whole amount by saying pay!(true)" do
    @invoice.pay!(true)
    expect(@invoice.amount_due).to eq 0
  end

  it "should not be payable if canceled" do
    @invoice.update(canceled: true)
    expect { @invoice.pay!(true) }.to raise_error(RuntimeError, 'Kan inte betala makulerad faktura')
  end

  it "should be payable by smaller amount" do
    invoice = @family3.invoices.create
    expect(invoice.amount_due).to eq 1311
    invoice.pay!(1200)
    expect(invoice.amount_due).to eq 111
  end

  it "should be cancelable" do
    jump(2015, 12, 1)
    invoice = @family3.invoices.create
    expect(invoice.canceled?).to be false
    invoice.cancel!
    expect(invoice.canceled?).to be true
  end

  it "should have scopes canceled and valid" do
    jump(2015, 11, 1)
    invoice = @family3.invoices.create
    expect(Invoice.canceled).to eq([])
    expect(Invoice.valid.count).to be 2
    invoice.cancel!
    expect(Invoice.canceled).to eq([invoice])
    expect(Invoice.valid.count).to be 1
  end

  it "should be replaceable" do
    invoice = @family3.invoices.create
    expect(invoice.canceled?).to be false
    invoice.replace!
    expect(invoice.canceled?).to be true
  end

  it "should be possible to send a notice, which touches last_reminded", vcr: {cassette_name: 'notice email fonts', allow_playback_repeats: true} do
    jump(2016, 1, 18)
    invoice = @family3.invoices.create
    expect(Invoice.not_recently_reminded.count).to be 2
    invoice.notice!
    expect(Invoice.not_recently_reminded.count).to be 1
  end

  it "should be possible to send a reminder, which touches last_reminded" do
    jump(2016, 2, 10)
    invoice = @family3.invoices.create
    expect(invoice.last_reminded).to be nil
    expect(invoice.recently_reminded?).to be false
    invoice.reminder!
    expect(invoice.last_reminded).to eq Date.today
    expect(invoice.recently_reminded?).to be true
  end

  it "should be possible to trigger an alert, which touches last_alert" do
    jump(2016, 3, 10)
    invoice = @family3.invoices.create
    expect(invoice.last_alert).to be nil
    invoice.alert!
    expect(invoice.last_alert).to eq Date.today
    expect(invoice.recently_reminded?).to be false
  end


  it "should notify, remind and alert about overdue invoices at particular intervals", vcr: {cassette_name: 'notice email fonts', allow_playback_repeats: true} do
    jump(2016, 4, 1)
    expect(Invoice.count).to be 1
    expect(Invoice.overdue.count).to be 1 # from earlier tests

    Invoice.create_for_families

    expect(Invoice.count).to be 3
    expect(Invoice.overdue.count).to be 1

    Invoice.send_notices_and_reminders # should not notify yet
    expect(Invoice.not_recently_reminded.count).to be 3

    jump(2016, 4, 2)
    Invoice.send_notices_and_reminders # should notify here!
    expect(Invoice.not_recently_reminded.count).to be 1

    jump(2016, 4, 26)
    expect(Invoice.overdue.count).to be 1 # on due date
    Invoice.send_notices_and_reminders # nothing should happen
    expect(waiting_count).to be 3

    jump(2016, 4, 27)
    expect(Invoice.overdue.count).to be 3 # after due date
    Invoice.send_notices_and_reminders # nothing should happen
    expect(Invoice.not_recently_reminded.count).to be 3

    invoice = @family3.invoices.valid.newest
    expect(invoice.overdue_days).to be 1

    invoice_payed = @family.invoices.valid.newest
    invoice_payed.pay!(true)
    expect(invoice_payed.last_reminded).to eq Date.new(2016,4,2)

    jump(2016, 4, 28)
    Invoice.send_notices_and_reminders # nothing should happen
    expect(waiting_count).to be 2

    jump(2016, 4, 29)
    Invoice.send_notices_and_reminders # nothing should happen
    expect(waiting_count).to be 2

    jump(2016, 5, 1)
    expect(Family.count).to be 3
    Invoice.create_for_families # a new month, as usual
    Invoice.send_notices_and_reminders # nothing should happen
    expect(waiting_count).to be 4

    jump(2016, 5, 2)
    expect(invoice.overdue_days).to be 6 # day before
    expect(Invoice.for_notices.count).to be 2
    Invoice.send_notices_and_reminders # should send 2 new notices
    expect(invoice.reload.last_reminded).to eq Date.new(2016,4,2)
    expect(waiting_count).to be 2

    payment_check!

    jump(2016, 5, 3)
    expect(invoice.overdue_days).to be 7 # first reminder!
    Invoice.send_notices_and_reminders
    expect(waiting_count).to be 1
    expect(invoice.reload.last_reminded).to eq Date.today

    expect(invoice_payed.reload.last_reminded).to eq Date.new(2016,4,2)
    expect(invoice_payed.reload.last_alert).to be nil

    jump(2016, 5, 10)
    expect(invoice.overdue_days).to be 14 # second reminder!
    Invoice.send_notices_and_reminders
    expect(waiting_count).to be 3
    expect(invoice.reload.last_reminded).to eq Date.today

    jump(2016, 5, 11)
    expect(invoice.overdue_days).to be 15 # day after
    Invoice.send_notices_and_reminders
    expect(waiting_count).to be 3
    expect(invoice.reload.last_reminded).to eq Date.yesterday

    payment_check!

    jump(2016, 5, 17)
    expect(invoice.overdue_days).to be 21 # first alert!
    Invoice.send_notices_and_reminders
    expect(waiting_count).to be 4
    expect(invoice.reload.last_alert).to eq Date.today
    expect(invoice.reload.last_reminded).to eq 7.days.ago.to_date

    jump(2016, 5, 18)
    expect(invoice.overdue_days).to be 22 # day after
    Invoice.send_notices_and_reminders
    expect(waiting_count).to be 4
    expect(invoice.reload.last_alert).to eq Date.yesterday
    expect(invoice.reload.last_reminded).to eq 8.days.ago.to_date

    expect(invoice_payed.reload.last_reminded).to eq Date.new(2016,4,2)
    expect(invoice_payed.reload.last_alert).to be nil
  end

  it "should not remind about overdue invoices if paymentcheck has not been successful lately", vcr: {cassette_name: 'notice email fonts', allow_playback_repeats: true} do
    jump(2016, 4, 1)
    payment_check!
    expect(Invoice.count).to be 1
    expect(Invoice.overdue.count).to be 1 # from earlier tests

    Invoice.create_for_families
    expect(Invoice.count).to be 3
    expect(Invoice.overdue.count).to be 1

    Invoice.send_notices_and_reminders # should not notify yet
    expect(Invoice.not_recently_reminded.count).to be 3
    expect(waiting_count).to be 3

    jump(2016, 4, 27)
    invoice = @family3.invoices.valid.newest
    expect(invoice.overdue_days).to be 1

    jump(2016, 5, 3)
    expect(invoice.overdue_days).to be 7 # first reminder!
    Invoice.send_notices_and_reminders
    expect(waiting_count).to be 3
    expect(invoice.reload.last_reminded).to be nil
  end

  it "should remind about overdue invoices if paymentcheck has been successful lately", vcr: {cassette_name: 'notice email fonts', allow_playback_repeats: true} do
    jump(2016, 4, 1)
    payment_check!
    expect(Invoice.count).to be 1
    expect(Invoice.overdue.count).to be 1 # from earlier tests

    Invoice.create_for_families
    expect(Invoice.count).to be 3
    expect(Invoice.overdue.count).to be 1

    Invoice.send_notices_and_reminders # should not notify yet
    expect(Invoice.not_recently_reminded.count).to be 3
    expect(waiting_count).to be 3

    jump(2016, 4, 27)
    invoice = @family3.invoices.valid.newest
    expect(invoice.overdue_days).to be 1

    payment_check! # if we haven't had a successful paymentcheck lately we don't send reminders, so the reminders are accurate

    jump(2016, 5, 3)
    expect(invoice.overdue_days).to be 7 # first reminder!
    Invoice.send_notices_and_reminders
    expect(waiting_count).to be 1
    expect(invoice.reload.last_reminded).to eq Date.today
  end


  def payment_check!
    PaymentCheck.create!(success: true)
  end

  def waiting_count
    Invoice.not_fully_payed.not_recently_reminded.valid.count
  end

  def jump(year, month, day)
    Timecop.travel(Time.local(year, month, day, 10, 1, 0))
  end

  def make_families
    @family = Family.create!(
      mother_name: "Lotta Husägare",
      mother_email: "lotta@test.se"
    )
    @kid = Kid.create!(
      family: @family,
      ssn: "20130102-0720",
      full_name: "Sune Rudolfsson",
      start_date: Date.new(2012,7,1)
    )
    @kid2 = Kid.create!(
      family: @family,
      ssn: "20131023-0720",
      full_name: "Adolf Butler",
      start_date: Date.new(2012,7,1)
    )
    @kid3 = Kid.create!(
      family: @family,
      ssn: "20140923-0720",
      full_name: "Svenne Björkstig",
      start_date: Date.new(2012,7,1)
    )
    @invoice = @family.invoices.create

    @family2 = Family.create!(
      mother_name: "Benny Andersson",
      mother_email: "benny@test.se",
    )

    @family3 = Family.create!(
      mother_name: "Anna Svensson",
      mother_email: "anna@test.se",
    )
    @f3kid = Kid.create!(
      family: @family3,
      ssn: "20140202-0721",
      full_name: "Håkan Bråkan",
      start_date: Date.new(2014,6,2)
    )
  end

  def clear_tables
    ActiveRecord::Base.connection.execute(
      'TRUNCATE invoices, kids, families CASCADE'
    )
  end
end
