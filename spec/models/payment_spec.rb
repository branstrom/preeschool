require 'rails_helper'

RSpec.describe Payment, type: :model do
  before(:all) do
    Timecop.return
    make_invoices
    make_payment_bundles
  end

  after(:all) do
    clear_tables
  end

  it 'should get the reference_ssn right for an old style ocr' do
    p = Payment.new(reference: '150217 1607')
    expect(p.reference_ssn).to eq '150217'
  end

  it 'should get the reference_fee_month right for an old style ocr' do
    p = Payment.new(reference: '150217 1607')
    expect(p.reference_fee_month).to eq Date.new(2016,7,1)
  end


  it 'should get the reference_ssn right when reference has no space in between' do
    p = Payment.new(reference: '1502171607')
    expect(p.reference_ssn).to eq '150217'
  end

  it 'should get the reference_fee_month right when reference has no space in between' do
    p = Payment.new(reference: '1502171607')
    expect(p.reference_fee_month).to eq Date.new(2016,7,1)
  end

  it 'should get the amount right' do
    p = Payment.new(amount: '900,00', from: 'Jultomten', reference: '150217 1607')
    expect(p.amount).to eq 900.0
  end

  it 'should have a human readable from_name' do
    p = Payment.new(from: 'BOSTRÖM, FRIDA')
    expect(p.from_name).to eq 'Frida Boström'

    p = Payment.new(from: 'JÖNSSON, JOHNNY ')
    expect(p.from_name).to eq 'Johnny Jönsson'

    p = Payment.new(from: 'ELMAZ REDJEPOVSKI')
    expect(p.from_name).to eq 'Elmaz Redjepovski'

    p = Payment.new(from: 'NORDLING,BERNT TOBIAS')
    expect(p.from_name).to eq 'Bernt Tobias Nordling'
  end

  it 'needs an amount' do
    p = Payment.new(from: 'Jultomten', reference: '150217 1607')
    p.valid?
    expect(p.errors.keys).to include(:amount)
  end

  it 'needs an given_on date' do
    p = Payment.new(from: 'Jultomten', reference: '150217 1607')
    p.valid?
    expect(p.errors.keys).to include(:given_on)
  end


  it 'should set invoice payed_on when created' do
    date = Date.new(2016,10,3)
    p = Payment.create!(amount: '1,50', from: 'Jultomten', reference: '604025098', given_on: date)
    expect(p.invoice.payed_on).to eq date
  end

  it 'should check whether we have not received payments lately' do
    expect(Payment.received_lately?).to eq(false)
  end

  it 'should check whether we have received payments lately' do
    date = Date.new(2016,10,3)
    p = Payment.create!(amount: '1,50', from: 'Jultomten', reference: '604025098', given_on: date)
    expect(Payment.received_lately?).to eq(true)
  end

  it 'should match to invoice based on parent name if need be' do
    date = Date.new(2016,10,6)
    unpayed_invoice = Family.where(mother_name: 'Maria Wallenman').first.invoices.valid.not_fully_payed.oldest
    p = Payment.create!(amount: '500', from: 'Maria Wallenman', reference: '', given_on: date)
    expect(p.invoice).not_to be_nil
    expect(p.invoice.payed_on).to eq date
    expect(p.invoice).to eq(unpayed_invoice)
  end

  it 'should get the overdue_days right' do
    p = Payment.create!(amount: '1,50', from: 'Jultomten', reference: '604026096', given_on: Date.new(2016,9,30))
    expect(p.overdue_days).to be 4
  end

  it 'should get negative overdue_days when early' do
    p = Payment.create!(amount: '1,50', from: 'Jultomten', reference: '604027185', given_on: Date.new(2016,10,20))
    expect(p.overdue_days).to be -15
  end

  it 'should fail when creating a single payment without an amount...' do
    expect { Payment.create!({}) }.to raise_error(ActiveRecord::RecordInvalid)
  end

  it '...but should be able to create payments from datewise bundles without failures' do
    expect {
      Payment.create_from_bundles!(@bundles)
    }.not_to raise_error
  end

  it 'should not allow saving duplicates' do
    Payment.create!(@bundles[Date.new(2016,10,18)][0].merge(given_on: Date.new(2016,10,18)))
    # ap @bundles[Date.new(2016,10,18)][0]
    expect { Payment.create!(@bundles[Date.new(2016,10,18)][0].merge(given_on: Date.new(2016,10,18))) }.to raise_error(ActiveRecord::RecordInvalid, /Amount används redan/)
  end

  it 'should not allow saving duplicates but should not throw errors when given bundles' do
    Payment.create_from_bundles!(@bundles)
    expect(Payment.count).to eq 6
    expect { Payment.create_from_bundles!(@bundles) }.not_to raise_error
    # ap Payment.all
    expect(Payment.count).to eq 6
  end

  it 'should interpret amount correctly (100+)' do
    row = @bundles[Date.new(2016,10,18)][0]
    expect(row[:amount]).to eq '795,00'
    payment = Payment.create!(row.merge(given_on: Date.new(2016,10,18)))
    expect(payment.amount).to eq 795.0
  end

  it 'should interpret amount correctly (1000+)' do
    row = @bundles[Date.new(2016,10,20)][0]
    expect(row[:amount]).to eq '2 703,00'
    payment = Payment.create!(row.merge(given_on: Date.new(2016,10,18)))
    expect(payment.amount).to eq 2703.0
  end

  it 'should interpret amount correctly (10000+ with decimals)' do
    row = @bundles[Date.new(2016,10,20)][1]
    expect(row[:amount]).to eq '318 630,19'
    payment = Payment.create!(row.merge(given_on: Date.new(2016,10,18)))
    expect(payment.amount).to eq 318630.19
  end

  def make_payment_bundles
    @bundles = {
      '2016-10-20': [
        {amount: "2 703,00", from: "Åsa Karlberg", reference: " ", avi_id: "493800087860"},
        {amount: "318 630,19", from: "Härnö sands Kommun", reference: " ", avi_id: "753-7699"}
      ],
      '2016-10-18': [
        {amount: "795,00", from: "ELMAZ REDJEPOVSKI", reference: "1412081609", avi_id: "493780014412"}],
      '2016-10-14': [
        {amount: "795,00", from: nil, reference: nil, avi_id: nil}
      ],
      '2016-10-13': [
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil}
      ],
      '2016-10-12': [
        {amount: "1 140,00", from: "JÖNSSON, JOHNNY", reference: "140215 1609", avi_id: "493720143034"}
      ],
      '2016-10-10': [
        {amount: "1 140,00", from: nil, reference: nil, avi_id: nil}
      ]
    }.transform_keys {|key| Date.parse(key.to_s) }
  end

  def make_invoices
    Family.new.from_json('{
      "id" : 10,
      "is_lead" : null,
      "father_phone" : null,
      "father_name" : "Per Dufvenberg",
      "created_at" : "2016-09-09 17:36:26.520744",
      "comment" : null,
      "parent_at_home" : false,
      "lead_comments" : null,
      "mother_email" : "maria_wallenman@hotmail.com",
      "income" : 43000,
      "updated_at" : "2016-10-07 21:47:07.956807",
      "mother_name" : "Maria Wallenman",
      "mother_phone" : null,
      "father_email" : null
    }').save!
    Kid.new.from_json('{
      "updated_at" : "2016-09-09 17:56:28.493769",
      "birth_order" : 1,
      "pending" : null,
      "id" : 16,
      "start_date" : "2011-10-06",
      "created_at" : "2016-09-09 17:37:29.153666",
      "family_id" : 10,
      "full_name" : "Aaron Dufvenberg",
      "ssn" : "20111006-0000",
      "end_date" : "2017-07-06",
      "comment" : null
    }').save!
    Kid.new.from_json('{
      "updated_at" : "2016-09-09 21:46:56.122219",
      "birth_order" : 2,
      "pending" : null,
      "id" : 21,
      "start_date" : "2014-09-01",
      "created_at" : "2016-09-09 17:56:28.500504",
      "family_id" : 10,
      "full_name" : "Jamie Dufvenberg",
      "ssn" : "20130702-0000",
      "end_date" : "2019-07-02",
      "comment" : null
    }').save!
    Invoice.new.from_json('{
      "id" : 33,
      "invoice_date" : "2016-10-10",
      "amount" : 1501.5,
      "amount_due" : 1501.5,
      "fee_month" : "2016-10-01",
      "last_reminded" : null,
      "canceled" : true,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604027185",
      "due_date" : "2016-11-04",
      "last_alert" : null
    }').save!
    Invoice.new.from_json('{
      "id" : 56,
      "invoice_date" : "2016-09-01",
      "amount" : 1501.5,
      "amount_due" : 1501.5,
      "fee_month" : "2016-09-01",
      "last_reminded" : null,
      "canceled" : null,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604026096",
      "due_date" : "2016-09-26",
      "last_alert" : null
    }').save!
    Invoice.new.from_json('{
      "id" : 79,
      "invoice_date" : "2016-08-01",
      "amount" : 976,
      "amount_due" : 2,
      "fee_month" : "2016-08-01",
      "last_reminded" : null,
      "canceled" : null,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604025098",
      "due_date" : "2016-08-26",
      "last_alert" : "2016-10-12"
    }').save!
    Invoice.new.from_json('{
      "id" : 99,
      "invoice_date" : "2016-10-10",
      "amount" : 1505,
      "amount_due" : 1505,
      "fee_month" : "2016-10-01",
      "last_reminded" : null,
      "canceled" : true,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604027185",
      "due_date" : "2016-11-04",
      "last_alert" : null
    }').save!
    Invoice.new.from_json('{
      "id" : 126,
      "invoice_date" : "2016-10-10",
      "amount" : 1505,
      "amount_due" : 1505,
      "fee_month" : "2016-10-01",
      "last_reminded" : "2016-10-16",
      "canceled" : null,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604027185",
      "due_date" : "2016-11-04",
      "last_alert" : null
    }').save!
  end

  def clear_tables
    ActiveRecord::Base.connection.execute(
      'TRUNCATE payments, invoices, kids, families CASCADE'
    )
  end
end
