require 'rails_helper'

RSpec.describe PaymentCheck, type: :model do
  it 'should be able to create a payment check' do
    p = PaymentCheck.new
    expect(p).not_to be_nil
  end

  it 'should default to not succeeded' do
    p = PaymentCheck.new
    expect(p.success).to be false
  end

  it 'should be able to mark it as succeeded' do
    p = PaymentCheck.new(success: true)
    expect(p.success).to be true
  end

  it 'should see that it has not been successful lately if last check failed' do
    PaymentCheck.create!(success: false)
    expect(PaymentCheck.successful_lately?).to be false
  end

  it 'should see that it has not been successful lately if last check succeeded but is too old' do
    jump(2015, 4, 1)
    PaymentCheck.create!(success: true)
    jump(2015, 4, 11)
    expect(PaymentCheck.successful_lately?).to be false
  end

  it 'should see that it has been successful lately' do
    PaymentCheck.create!(success: false)
    PaymentCheck.create!(success: true)
    expect(PaymentCheck.successful_lately?).to be true
  end

  it 'should be created when actually checking and succeeding' do
    expect(BankSpider).to receive(:run).and_return(make_payment_bundles)
    expect(PaymentCheck.last).to be nil
    Payment.try_to_fetch_new!
    expect(PaymentCheck.last).not_to be nil
    expect(PaymentCheck.last.success?).to be true
  end

  it 'should be created when actually checking and failing' do
    expect(BankSpider).to receive(:run).and_return({})
    expect(PaymentCheck.last).to be nil
    Payment.try_to_fetch_new!
    expect(PaymentCheck.last).not_to be nil
    expect(PaymentCheck.last.success?).to be false
  end


  def jump(year, month, day)
    Timecop.travel(Time.local(year, month, day, 10, 1, 0))
  end


  def make_payment_bundles
    @bundles = {
      '2016-10-20': [
        {amount: "2 703,00", from: "Åsa Karlberg", reference: " ", avi_id: "493800087860"},
        {amount: "318 630,19", from: "Härnö sands Kommun", reference: " ", avi_id: "753-7699"}
      ],
      '2016-10-18': [
        {amount: "795,00", from: "ELMAZ REDJEPOVSKI", reference: "1412081609", avi_id: "493780014412"}],
      '2016-10-14': [
        {amount: "795,00", from: nil, reference: nil, avi_id: nil}
      ],
      '2016-10-13': [
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil},
        {amount: nil, from: nil, reference: nil, avi_id: nil}
      ],
      '2016-10-12': [
        {amount: "1 140,00", from: "JÖNSSON, JOHNNY", reference: "140215 1609", avi_id: "493720143034"}
      ],
      '2016-10-10': [
        {amount: "1 140,00", from: nil, reference: nil, avi_id: nil}
      ]
    }.transform_keys {|key| Date.parse(key.to_s) }
  end


end
