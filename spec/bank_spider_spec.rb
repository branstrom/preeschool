# require "spec_helper"
require 'rails_helper'

RSpec.describe BankSpider do
  around(:each) do |example|
    WebMock.allow_net_connect!
    VCR.turned_off { example.run }
    WebMock.disable_net_connect!
  end

  before(:all) do
    Timecop.return
    make_invoices
  end

  after(:all) do
    clear_tables
  end

  it "should be able to log in using browser", vcr: false do
    skip 'doesnt work due to qr code for bankid these days'
    # result = BankSpider.login_and_fetch_payments
    result = BankSpider.run
    ap result
    expect(result).not_to be_nil
  end

  it 'should test puffing billy', vcr: false do
    skip 'this works'
    proxy.stub('http://www.google.com/').and_return(:text => "I'm not Google!")
    browser = Billy::Browsers::Watir.new :firefox
    browser.goto 'http://www.google.com/'
    expect(browser.text).to eq("I'm not Google!")
  end

  it "should be able to log in and process bank payments", vcr: false do
    skip 'puffing billy specs not fully working yet'
    puts "Persistent cache?"
    puts Billy.config.persist_cache
    puts "Cache path?"
    puts Billy.config.cache_path
    puts proxy

    proxy.stub('https://online.swedbank.se/app/ib/logga-in').and_return(:text => 'Foobar')

    capabilities = Selenium::WebDriver::Remote::Capabilities.firefox(accept_insecure_certs: true)
    browser = Billy::Browsers::Watir.new(:firefox, capabilities: capabilities)
    # browser = Billy::Browsers::Watir.new(:chrome, options: {proxy: proxy}, switches: nil)
    WatirAngular.inject_wait(browser)

    result = BankSpider.login_and_fetch_payments(browser)
    ap result
    expect(result).not_to be_nil
  end


  # it "should be able to test bankgiro processing locally" do #, vcr: true
  #   result = BankSpider.test_process
  #   ap result
  #   expect(result).not_to be_nil
  # end


  # TODO: decide on whether use of VCR here is needed/wise


  it "should be able to do payments matching" do #, vcr: true
    skip 'geckodriver needs fixing'
    # pending 'awaiting more recent data from cached request'
    # fail
    @bundles = BankSpider.test_process
    bundle = @bundles[Date.new(2016,10,18)] #
    # ap bundle
    payment = Payment.create!(bundle.first.merge(given_on: Date.new(2016,10,18)))
    # ap payment
    # ap payment.invoice
    expect(payment).not_to be_nil
    expect(payment.persisted?).to be true
    expect(payment.invoice).not_to be_nil
    expect(payment.invoice.payed_on).to eq Date.new(2016,10,18)
    expect(payment.invoice.amount_due).to eq 1165.5
  end

  it "should be able to do payments matching in bundles" do #, vcr: true
    skip 'geckodriver needs fixing'
    # pending 'awaiting more recent data from cached request'
    # fail
    @bundles = BankSpider.test_process
    Payment.create_from_bundles!(@bundles)
    expect(Payment.count).to be 17

    Payment.all.each do |payment|
      if payment.invoice && payment.invoice.not_fully_payed?
        expect(payment.invoice.payed_on).to eq payment.given_on
        expect(payment.invoice.amount_due).to be >= 0
      else
        expect(payment.reference).to be_truthy # match /\d{6} \d{4}|\d+/
      end
    end
    payed = Payment.where('invoice_id IS NOT NULL')
    # ap payed
    expect(payed.count).to eq 4
  end

  it "should ignore duplicate payments" do #, vcr: true
    skip 'geckodriver needs fixing'
    # pending 'awaiting more recent data from cached request'
    # fail
    @bundles = BankSpider.test_process
    expect {
      Payment.create_from_bundles!(@bundles)
    }.not_to raise_error
    # ap Payment.all
    expect(Payment.count).to be 17
  end


  def make_invoices
    Family.new.from_json('{
      "id" : 10,
      "is_lead" : null,
      "father_phone" : null,
      "father_name" : "Per Dufvenberg",
      "created_at" : "2016-09-09 17:36:26.520744",
      "comment" : null,
      "parent_at_home" : false,
      "lead_comments" : null,
      "mother_email" : "maria_wallenman@hotmail.com",
      "income" : 43000,
      "updated_at" : "2016-10-07 21:47:07.956807",
      "mother_name" : "Maria Wallenman",
      "mother_phone" : null,
      "father_email" : null
    }').save!
    Kid.new.from_json('{
      "updated_at" : "2016-09-09 17:56:28.493769",
      "birth_order" : 1,
      "pending" : null,
      "id" : 16,
      "start_date" : "2011-10-06",
      "created_at" : "2016-09-09 17:37:29.153666",
      "family_id" : 10,
      "full_name" : "Aaron Dufvenberg",
      "ssn" : "20111006-0000",
      "end_date" : "2017-07-06",
      "comment" : null
    }').save!
    Kid.new.from_json('{
      "updated_at" : "2016-09-09 21:46:56.122219",
      "birth_order" : 2,
      "pending" : null,
      "id" : 21,
      "start_date" : "2014-09-01",
      "created_at" : "2016-09-09 17:56:28.500504",
      "family_id" : 10,
      "full_name" : "Jamie Dufvenberg",
      "ssn" : "20130702-0000",
      "end_date" : "2019-07-02",
      "comment" : null
    }').save!
    Invoice.new.from_json('{
      "id" : 33,
      "invoice_date" : "2016-10-10",
      "amount" : 1501.5,
      "amount_due" : 1501.5,
      "fee_month" : "2016-10-01",
      "last_reminded" : null,
      "canceled" : true,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604027185",
      "due_date" : "2016-11-04",
      "last_alert" : null
    }').save!

    # THIS ONE will get payed above
    Invoice.new.from_json('{
      "id" : 56,
      "invoice_date" : "2016-09-01",
      "amount" : 1501.5,
      "amount_due" : 1501.5,
      "fee_month" : "2016-09-01",
      "last_reminded" : null,
      "canceled" : null,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604026096",
      "due_date" : "2016-09-26",
      "last_alert" : null
    }').save!

    Invoice.new.from_json('{
      "id" : 79,
      "invoice_date" : "2016-08-01",
      "amount" : 976,
      "amount_due" : 0,
      "fee_month" : "2016-08-01",
      "last_reminded" : null,
      "canceled" : null,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604025098",
      "due_date" : "2016-08-26",
      "last_alert" : "2016-10-12"
    }').save!
    Invoice.new.from_json('{
      "id" : 99,
      "invoice_date" : "2016-10-10",
      "amount" : 1505,
      "amount_due" : 1505,
      "fee_month" : "2016-10-01",
      "last_reminded" : null,
      "canceled" : true,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604027185",
      "due_date" : "2016-11-04",
      "last_alert" : null
    }').save!
    Invoice.new.from_json('{
      "id" : 126,
      "invoice_date" : "2016-10-10",
      "amount" : 1505,
      "amount_due" : 1505,
      "fee_month" : "2016-10-01",
      "last_reminded" : "2016-10-16",
      "canceled" : null,
      "payed_on" : null,
      "family_id" : 10,
      "ocr" : "604027185",
      "due_date" : "2016-11-04",
      "last_alert" : null
    }').save!
  end

  def clear_tables
    ActiveRecord::Base.connection.execute(
      'TRUNCATE payments, invoices, kids, families CASCADE'
    )
  end
end
