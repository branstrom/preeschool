# encoding: utf-8
require 'rails_helper'

Rails.application.load_tasks

RSpec.describe "import google sheet" do
  before(:each) do
    Timecop.return # Google API demands accurate system clock
    # ap Date.today
  end

  before(:all) do
    expect(Kid.count).to be 0

    VCR.use_cassette('import google sheet', {allow_playback_repeats: true }) do
      expect {
        Rake::Task["import:google_sheet"].invoke
      }.not_to raise_error
    end

    expect(Kid.count).to be 2
  end

  # after(:each) do
  #   Rake::Task["import:google_sheet"].reenable
  #   # clear_tables
  # end

  # it 'reads, interprets and creates records', vcr: {
  #   cassette_name: 'import google sheet',
  #   allow_playback_repeats: true
  # } do
  #   expect(Kid.count).to be 0

  #   expect {
  #     Rake::Task["import:google_sheet"].invoke
  #   }.not_to raise_error

  #   expect(Kid.count).to be 2
  # end

  it "it creates kid with correct start_date" do
    expect(Kid.find_by_ssn('20150206-0000').start_date).to eq Date.new(2016,2,6)
  end

  context 'reading AFSK correctly' do
    it "creates kid with AFSK true" do
      kid = Kid.find_by_ssn('20150206-0000')
      expect(Kid.find_by_ssn('20150206-0000').free_15_hour_week).to be_truthy
    end

    it "creates kid with AFSK false" do
      expect(Kid.find_by_ssn('20190416-0000').free_15_hour_week).to be_falsy
    end
  end


  def clear_tables
    ActiveRecord::Base.connection.execute(
      'TRUNCATE invoices, kids, families CASCADE'
    )
  end
end