# require "spec_helper"
require 'rails_helper'

RSpec.describe InfoMailer do
  before(:all) do
    @family = Family.create!(
      mother_name: "Lotta Husägare",
      mother_email: "lotta@test.se",
      father_name: 'Stefan Husägare',
      father_email: 'stefan@test.se'
    )
    @kid = Kid.create!(
      family: @family,
      ssn: "20130102-0720",
      full_name: "Sune Rudolfsson",
      start_date: Date.new(2012,7,1)
    )
  end

  it "should find all active kids' families' emails" do
    expect(Kid.has_family).to eq [@kid]
    expect(InfoMailer.parent_emails).to eq ['lotta@test.se', 'stefan@test.se']
    @family2 = Family.create!(
      mother_name: "Angela Merkel",
      mother_email: "merkel@test.se",
      father_name: 'Stefan Löfven',
      father_email: 'lofven@test.se'
    )
    @kid2 = Kid.create!(
      family: @family2,
      ssn: "20130103-0721",
      full_name: "Ett Annat Barn",
      start_date: Date.new(2012,7,2)
    )
    expect(Kid.has_family).to eq [@kid, @kid2]
    expect(InfoMailer.parent_emails).to eq ['lotta@test.se', 'stefan@test.se', 'merkel@test.se', 'lofven@test.se']
    expect(InfoMailer.all_emails).to eq ['lotta@test.se', 'stefan@test.se', 'merkel@test.se', 'lofven@test.se', 'info@forskolan-solsidan.se']
  end

  it "should be able to send an information email", vcr: {cassette_name: 'information email fonts'} do
    message = Message.create!(
      subheader: 'Hej hå',
      text: 'Vi till vår gruva gå'
    )
    m = InfoMailer.information(message)
    expect(m.to).to eq InfoMailer.all_emails
    expect(m.body.encoded).to match('Information fr')
    expect { m.deliver_now }
      .to change { ActionMailer::Base.deliveries.count }.by(1)
  end
  it "should be able to send an information email to a particular family", vcr: {cassette_name: 'information email fonts'} do
    message = Message.create!(
      subheader: 'Hej hå',
      text: 'Vi till vår gruva gå',
      family: @family
    )
    m = InfoMailer.information(message)
    expect(m.to).to eq @family.emails
    expect(m.body.encoded).to match(@family.surnames)
    # expect { m.deliver_now }
    #   .to change { ActionMailer::Base.deliveries.count }.by(1)
  end
end
