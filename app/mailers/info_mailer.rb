class InfoMailer < ActionMailer::Base
  layout 'email'

  default from: "info@forskolan-solsidan.se"

  OUR_EMAILS = ['info@forskolan-solsidan.se']

  def information(message)
    return false unless message
    @message = message
    addresses = @message.family&.emails || InfoMailer.all_emails
    mail(
      to: addresses,
      # bcc: ['branstrom@gmail.com'],
      subject: Message::HEADER
    )
  end

  # protected

  def self.all_emails
    [self.parent_emails, OUR_EMAILS].flatten
  end

  def self.parent_emails
    Kid.has_family.active.map(&:family).map do |family|
      family.emails
    end.flatten.uniq
  end
end
