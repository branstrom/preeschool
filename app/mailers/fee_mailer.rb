class FeeMailer < ActionMailer::Base
  layout 'email'

  default from: "info@forskolan-solsidan.se"

  OUR_EMAILS = ['info@forskolan-solsidan.se']

  def notice(invoice)
    load_invoice(invoice)
    return if @invoice.recently_reminded?

    attachments[@pdf.filename] = @pdf.render
    mail(
      to: @family.emails,
      bcc: OUR_EMAILS, #.concat(['branstrom@gmail.com']),
      subject: "Månadsfaktura #{@invoice.fee_month_human}"
    )
  end

  def reminder(invoice)
    load_invoice(invoice)
    return unless @invoice.overdue_days > 4

    attachments[@pdf.filename] = @pdf.render
    mail(
      to: @family.emails,
      bcc: OUR_EMAILS, #.concat(['branstrom@gmail.com']),
      subject: "Påminnelse Månadsfaktura #{@invoice.fee_month_human}"
    )
  end

  def alert(invoice)
    load_invoice(invoice)
    return unless @invoice.overdue_days > 14

    attachments[@pdf.filename] = @pdf.render
    mail(
      to: OUR_EMAILS,
      # bcc: ['branstrom@gmail.com'],
      subject: "Försenad Månadsfaktura #{@family.name} #{@invoice.fee_month_human}"
    )
  end

  def payments_fetch_alert
    mail(
      to: OUR_EMAILS,
      # bcc: ['branstrom@gmail.com'],
      subject: "Logga in med BankID för att hämta inbetalningar"
    )
  end

  protected

  def load_invoice(invoice)
    @family = invoice.family
    @kids = @family.kids.active
    @invoice = invoice || @family.invoices.newest
    @pdf = InvoicePdf.new(@invoice, view_context)
  end
end
