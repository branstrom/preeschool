class FeeMailerPreview

  def reminder
    @family = Family.find(@family_id) if @family_id
    @invoice = Invoice.find(@invoice_id) if @invoice_id
    @invoice ||= @family.invoices.oldest
    I18n.locale = :sv # FIXME: attachments don't inherit locale?
    FeeMailer.reminder @invoice
  end

  def notice
    @family = Family.find(@family_id) if @family_id
    @invoice = Invoice.find(@invoice_id) if @invoice_id
    @invoice ||= @family.invoices.oldest
    I18n.locale = :sv
    FeeMailer.notice @invoice
  end

  def alert
    @family = Family.find(@family_id) if @family_id
    @invoice = Invoice.find(@invoice_id) if @invoice_id
    @invoice ||= @family.invoices.oldest
    I18n.locale = :sv
    FeeMailer.alert @invoice
  end

  def payments_fetch_alert
    I18n.locale = :sv
    FeeMailer.payments_fetch_alert
  end


  private
  # You can put all your mock helpers in a module
  # or you can use your factories / fabricators, just make sure you are not creating anything

end
