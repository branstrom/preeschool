class InfoMailerPreview

  def information
    # @family = Family.find(@family_id) if @family_id
    @message = Message.find(@message_id) if @message_id

    I18n.locale = :sv # FIXME: attachments don't inherit locale?

    InfoMailer.information @message
  end

end