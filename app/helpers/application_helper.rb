module ApplicationHelper

  DEFAULT_KEY_MATCHING = {
    alert:     :alert,
    notice:    :success,
    info:      :primary,
    secondary: :secondary,
    success:   :success,
    error:     :alert
  }

  def display_flash_messages(key_matching = {})
    key_matching = DEFAULT_KEY_MATCHING.merge(key_matching)

    flash.inject "" do |message, (key, value)|
      message += content_tag :div, data: { alert: "" }, class: "callout #{key_matching[key.to_sym] || :primary}" do
      (value + link_to("&times;".html_safe, "#", class: :close)).html_safe
      end
    end.html_safe
  end


  def reminder_link(invoice)
    render inline: <<-HAML.strip_heredoc, type: :haml, locals: { invoice: invoice }
      = link_to reminder_admin_invoice_path(invoice), method: :post, data: { confirm: 'Säker på att du vill skicka en påminnelse om fakturan?' }, class: 'btn btn-danger', title: "Skicka en mail-påminnelse om denna faktura (senast notifierad: #{invoice.last_reminded ? I18n.l(invoice.last_reminded, format: :short) : 'aldrig'})" do
        %span.glyphicon.glyphicon-fire
        Påminn
    HAML
  end
  def notice_link(invoice)
    render inline: <<-HAML.strip_heredoc, type: :haml, locals: { invoice: invoice }
      = link_to notice_admin_invoice_path(invoice), method: :post, data: { confirm: 'Säker på att du vill skicka en notis om senaste fakturan?' }, class: 'btn btn-warning', title: "Skicka en mail-notis om denna faktura (senast notifierad: #{invoice.last_reminded ? I18n.l(invoice.last_reminded, format: :short) : 'aldrig'})" do
        %span.glyphicon.glyphicon-flag
        Notis
    HAML
  end
  def admin_links(invoice)
    render inline: <<-HAML.strip_heredoc, type: :haml, locals: { invoice: invoice }
      = link_to mark_as_payed_admin_invoice_path(invoice), method: :post, data: { confirm: 'Säker på att du vill markera denna faktura som betald?' }, class: 'btn btn-success', title: 'Markera denna faktura som betald' do
        %span.glyphicon.glyphicon-ok-circle
        Betald

      = link_to cancel_admin_invoice_path(invoice), method: :post, data: { confirm: 'Säker på att du vill makulera denna faktura?' }, class: 'btn btn-warning', title: 'Makulera denna faktura' do
        %span.glyphicon.glyphicon-trash
        Makulera

      = link_to replace_admin_invoice_path(invoice), method: :post, data: { confirm: 'Säker på att du vill ersätta denna faktura med en som skapats utifrån senaste datan? Ev. delbetalning kommer att återappliceras.' }, class: 'btn btn-default', title: 'Ersätt denna faktura med en som skapats utifrån senaste datan. Finns delbetalning återappliceras den.' do
        %span.glyphicon.glyphicon-repeat
        Ersätt
    HAML
  end
end
