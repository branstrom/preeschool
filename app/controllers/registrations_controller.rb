class RegistrationsController < ApplicationController

  before_action :build_family,  :only => [:new, :create]
  before_action :load_family,   :only => [:edit, :update]

  def new
    @family.kids.build
    render
  end

  def edit
    render
  end

  def create
    @family.kids.first.is_lead = true # TODO create migration, also add pickup/arrival times
    @family.save!
    flash[:success] = 'Intresseanmälan skapades'
    redirect_to :action => :edit, :id => @family
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Kunde inte skapa intresseanmälan'
    render :action => :new
  end

  def update
    @family.update_attributes!(family_params)
    flash[:success] = 'Intresseanmälan uppdaterades'
    redirect_to :action => :edit, :id => @family
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Kunde inte uppdatera intresseanmälan'
    render :action => :edit
  end

protected

  def build_family
    @form_path = registrations_path
    @family = Family.new(family_params)
    @family.is_lead = true
  end

  def load_family
    @family = Family
      .includes(:kids).references(:kids)
      .where(is_lead: true)
      .find(params[:id])
    @form_path = registration_path(@family)
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = "Ingen intresseanmälan med id #{params[:id]} kunde hittas"
    redirect_to action: :new # Hmm, where to go
  end

  def family_params
    params.fetch(:family, {}).permit(:id, :mother_name, :father_name, :mother_email, :father_email, :mother_phone, :father_phone, :income, :parent_at_home, :lead_comments, kids_attributes: [:id, :full_name, :ssn, :start_date])
  end
end