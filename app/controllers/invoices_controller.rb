class InvoicesController < ApplicationController

  def show
    @invoice = Invoice
      .includes(family: :kids).references(family: :kids)
      .find(params[:id])

    #format.html # TODO: use mailer template with link to PDF?
    respond_to do |format|
      format.json { render json: @invoice }
      format.pdf do
        pdf = InvoicePdf.new(@invoice, view_context)
        send_data pdf.render,
          filename: pdf.filename,
          type: "application/pdf",
          disposition: 'inline'
=begin
        if File.exist?(@invoice.file_path)
          send_file @invoice.file_path, :type => "application/pdf", :disposition => 'inline'
        else
          #redirect_to :action => :edit
          flash[:error] = "Faktura har inte genererats."
        end
=end
      end

    end
  end

end
