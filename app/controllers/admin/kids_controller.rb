class Admin::KidsController < Comfy::Admin::Cms::BaseController

  before_action :build_kid,  :only => [:new, :create]
  before_action :load_kid,   :only => [:show, :edit, :update, :destroy]

  def index
    # TODO: exclude "expired" kids (>3 months since end date)
    # with a button to show all
    @kids = Kid.order(full_name: :asc)
    if params[:order].present?
      @kids = Kid.order(params[:order].to_sym)
    end
    @kids = @kids.includes(:family).references(:family)
      .page(params[:page])
    if params[:name].present?
      @kids = @kids.by_name(params[:name]).order(full_name: :desc)
      redirect_to edit_admin_kid_path(@kids.first) if @kids.count == 1
    end
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @kid.save!
    flash[:success] = 'Barn skapades'
    redirect_to :action => :edit, :id => @kid
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Kunde ej skapa barn'
    render :action => :new
  end

  def update
    @kid.update_attributes!(kid_params)
    flash[:success] = 'Barn uppdaterades'
    redirect_to :action => :edit, :id => @kid
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Kunde ej uppdatera barn'
    render :action => :edit
  end

  def destroy
    @kid.destroy
    flash[:success] = 'Barn togs bort'
    redirect_to :action => :index
  end

protected

  def build_kid
    first_names = %w(Georg Elfrida Rutger Katrin Gustaf Caroline Vilhelm Sara)
    last_names = %w(Augustenborg Messenius Moberg Almqvist Adlersparre Adlercreutz Månsdotter)
    @random_name = "#{first_names.sample} #{last_names.sample}"
    @random_ssn = "#{(1...3).to_a.sample.years.ago.to_date.to_s.gsub(/\D/, '')}-0405"

    @kid = Kid.new(kid_params)
  end

  def load_kid
    @kid = Kid
      .includes(:family).references(:family)
      .find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = "Inget barn med id #{params[:id]} kunde hittas"
    redirect_to :action => :index
  end

  def kid_params
    params.fetch(:kid, {}).permit(:full_name, :ssn, :start_date, :end_date, :comment, :family_id, :pending)
  end
end