class Admin::InvoicesController < Comfy::Admin::Cms::BaseController

  # TODO: caching?

  before_action :load_invoice, only: [
    :notice, :reminder, :alert, :mark_as_payed, :replace, :cancel
  ]

  def index
    @overdue = Invoice.includes(family: :kids).references(family: :kids)
      .overdue
      .valid.by_date_asc.by_amount_due

    @overdue_page = @overdue.page(params[:page])

    @long_overdue = @overdue.long_overdue

    @all_paid_invoices = Invoice.includes(:payments).references(:payments)
      .fully_payed
      .valid.by_payed_on

    @paid_invoices = @all_paid_invoices.limit(12)

    @unmatched_payments = Payment
      .where(invoice: nil).where.not(from: 'Härnösands Kommun')
      .order(given_on: :desc).limit(12)

    @new_invoices = Invoice.includes(family: :kids).references(family: :kids)
      .not_overdue.not_recently_reminded
      .valid.by_date_asc.by_amount_due
  end

  def check_payments
    result = Payment.try_to_fetch_new!
    logger.info result
    flash[:success] = "Hämtade #{result &.count} nya betalningar (under hela detta dygn)."
  rescue => e
    logger.error e.message
    flash[:danger] = "Kunde inte hämta betalningar: #{e}"
  ensure
    # render json: result
    redirect_to action: :index
  end

  def notice
    @invoice.notice!
    flash[:success] = "Skickade notis om faktura #{@invoice.id}"
  rescue => e
    logger.error e.message
    flash[:danger] = "Notis kunde inte skickas: #{e}"
  ensure
    redirect_to action: :index
  end

  def reminder
    @invoice.reminder!
    flash[:success] = "Skickade påminnelse om faktura #{@invoice.id}"
  rescue => e
    logger.error e.message
    flash[:danger] = "Påminnelse kunde inte skickas: #{e}"
  ensure
    redirect_to action: :index
  end

  def alert
    @invoice.alert!
    flash[:success] = "Skickade signal om obetald faktura #{@invoice.id}"
  rescue => e
    logger.error e.message
    flash[:danger] = "Signal kunde inte skickas: #{e}"
  ensure
    redirect_to action: :index
  end

  def mark_as_payed
    @invoice.pay!(true)
    flash[:success] = "Markerade faktura #{@invoice.id} som betald"
  rescue => e
    logger.error e.message
    flash[:danger] = "Faktura kunde inte markeras som betald: #{e}"
  ensure
    redirect_to action: :index
  end

  def replace
    @invoice.replace!
    flash[:success] = "Återskapade faktura #{@invoice.id} utifrån senaste data"
  rescue => e
    logger.error e.message
    flash[:danger] = "Faktura kunde inte ersättas/återskapas: #{e}"
  ensure
    redirect_to action: :index
  end

  def cancel
    @invoice.cancel!
    flash[:success] = "Makulerade faktura #{@invoice.id}"
  rescue => e
    logger.error e.message
    flash[:danger] = "Faktura kunde inte makuleras: #{e}"
  ensure
    redirect_to action: :index
  end

protected

  def load_invoice
    @invoice = Invoice.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Faktura hittades ej'
    redirect_to action: :index
  end

end
