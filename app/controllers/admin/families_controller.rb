class Admin::FamiliesController < Comfy::Admin::Cms::BaseController

  # TODO: caching?

  before_action :build_family,  :only => [:new, :create]
  before_action :load_family,   :only => [:show, :edit, :update, :destroy]

  def index
    # TODO: order by youngest kid
    # TODO: exclude "expired" kids (>3 months since end date)
    @families = Family.order('kids.start_date DESC')
      .non_leads
      .includes(:kids).references(:kids)
      .page(params[:page])
  end

  def leads
    # TODO: order by youngest kid
    # TODO: exclude "expired" kids (>3 months since end date)
    @leads = Family.order(:created_at)
      .leads
      .includes(:kids).references(:kids)
      .page(params[:page])
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    @invoice = @family.invoices.includes(family: :kids).references(family: :kids)
      .not_fully_payed.valid.oldest

    @invoices = @family.invoices.includes(family: :kids).references(family: :kids)
      .valid.by_date.limit(12)

    render
  end

  def create
    @family.save!
    flash[:success] = 'Familj skapades'
    redirect_to :action => :edit, :id => @family
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Failed to create Family'
    render :action => :new
  end

  def update
    @family.update_attributes!(family_params)
    flash[:success] = 'Familj uppdaterades'
    redirect_to :action => :edit, :id => @family
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Kunde ej uppdatera familj'
    render :action => :edit
  end

  def destroy
    @family.destroy
    flash[:success] = 'Familj togs bort'
    redirect_to :action => :index
  end

protected

  def build_family
    @family = Family.new(family_params)
  end

  def load_family
    @family = Family
      .includes(:kids, :invoices).references(:kids, :invoices)
      .find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = "Ingen familj med id #{params[:id]} kunde hittas"
    redirect_to :action => :index
  end

  def family_params
    params.fetch(:family, {}).permit(:mother_name, :father_name, :mother_email, :father_email, :mother_phone, :father_phone, :income, :comment, :parent_at_home)
  end
end