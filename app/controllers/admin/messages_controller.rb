class Admin::MessagesController < Comfy::Admin::Cms::BaseController

  before_action :build_message,  only: [:new, :create]
  before_action :load_message,   only: [:show]

  def index
    @messages = Message.order(created_at: :desc)
      .page(params[:page])
  end

  def show
    render
  end

  def new
    render
  end

  def create
    @message.save!
    if @message.persisted?
      InfoMailer.information(@message).deliver_now
    end
    flash[:success] = 'Meddelande skickades och loggades'
    redirect_to action: :show, id: @message
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Kunde ej skapa meddelande'
    render action: :new
  end

protected

  def build_message
    @message = Message.new(message_params)
  end

  def load_message
    @message = Message.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = "Inget meddelande med id #{params[:id]} kunde hittas"
    redirect_to action: :index
  end

  def message_params
    params.fetch(:message, {}).permit(:family_id, :subheader, :text)
  end
end