class Admin::FinancesController < Comfy::Admin::Cms::BaseController

  def index
    @kommun_payments = Payment.where(from: 'Härnösands Kommun')
      .order(given_on: :desc).limit(12)

	@younger_kids, @older_kids = Kid.active.partition {|kid| kid.subsidy_young? }
  end

end
