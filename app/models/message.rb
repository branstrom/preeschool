class Message < ActiveRecord::Base
  belongs_to :family

  # has_paper_trail # https://github.com/airblade/paper_trail

  validates_presence_of :text, :subheader

  HEADER = 'Information från Förskolan Solsidan'

end
