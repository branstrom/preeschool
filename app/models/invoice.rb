
class Invoice < ActiveRecord::Base
  RECENT = 5.days

  belongs_to :family
  has_many :payments

  has_paper_trail # https://github.com/airblade/paper_trail

  validates_presence_of :family, :invoice_date, :amount

  validates_uniqueness_of :fee_month, scope: :family_id, conditions: -> { valid }
  validates_uniqueness_of :invoice_date, scope: :family_id, conditions: -> { valid }

  validates_uniqueness_of :ocr, conditions: -> { valid }

  before_validation :set_invoice_date, on: [:create]
  before_validation :set_due_date, on: [:create, :update]
  before_validation :set_fee_month, on: [:create]
  before_validation :filter_summer_months, on: [:create]
  before_validation :set_amount, on: [:create, :update]
  before_validation :set_ocr, on: [:create, :update]

  ### SCOPES

  scope :by_amount_due, -> { order(amount_due: :desc) }

  scope :by_date, -> { order(invoice_date: :desc) }
  scope :by_date_asc, -> { order(invoice_date: :asc) }

  # Emulating NULLS LAST below, see http://stackoverflow.com/a/33047654/483020
  scope :by_payed_on, -> { order("payed_on IS NULL ASC").order(payed_on: :desc) }
  scope :by_payed_on_asc, -> { order(payed_on: :asc) }

  scope :canceled, -> { where(canceled: true) }
  scope :valid, -> { where('NOT(canceled IS true)') }

  scope :not_fully_payed, -> { where("amount_due > 0") }
  scope :fully_payed, -> { where("amount_due <= 0.0") }

  scope :overdue, lambda { |date = Date.today| not_fully_payed.where("due_date < ?", date) }
  scope :not_overdue, lambda { |date = Date.today| not_fully_payed.where("due_date >= ?", date) }
  scope :was_overdue_days_average, -> { fully_payed.average("payed_on - due_date").to_f }

  scope :recently_reminded, -> { where("last_reminded >= ?", RECENT.ago) }
  scope :not_recently_reminded, -> { where("last_reminded IS NULL OR last_reminded < ?", RECENT.ago) }
  scope :not_reminded, -> { where(last_reminded: nil) }


  def self.newest
    by_date.first
  end
  def self.oldest
    by_date.last
  end

  def self.long_overdue
    overdue(30.days.ago)
  end

  def long_overdue?
    overdue_days >= 30
  end

  def recently_reminded?
    self.last_reminded? && self.last_reminded >= RECENT.ago
  end
  def not_recently_reminded?
    !recently_reminded?
  end

  def status_text
    "Senaste notis/påminnelse: #{last_reminded ? I18n.l(last_reminded, format: :short) : 'aldrig'}
Senaste interna signal: #{last_alert ? I18n.l(last_alert, format: :short) : 'aldrig'}"
  end

  def status_text_paid
    given_on = payments.last&.given_on
    "#{'Kan vara överbetald!

' if overpayed?}Antal inbetalningar: #{payments.count}
Totalt inbetalat: #{payments.sum(:amount).to_i || '-'} kr
Referens: #{payments.last&.reference || '-'}
Inkom: #{given_on && I18n.l(given_on, format: :short) || '-'}
Markerad som betald: #{payed_on && I18n.l(payed_on, format: :short) || '-'}"
  end

  def overdue?
    not_fully_payed? &&
    self.due_date < Date.today
  end

  def overdue_days
    return 0 unless overdue?
    (Date.today - self.due_date).to_i
  end
  def was_overdue_days
    return 0 unless self.payed_on
    (self.payed_on - self.due_date).to_i
  end

  def overdue_duration(days = overdue_days)
    Invoice.overdue_duration(days)
  end
  def self.overdue_duration(days)
    return '' if days.zero?
    days.date_duration
  end

  def was_overdue_duration
    overdue_duration(was_overdue_days)
  end
  def self.was_overdue_duration_average
    avg = was_overdue_days_average
    avg > 0 ? overdue_duration(avg) : '0 dagar'
  end

  def fee_month_human
    I18n.l(fee_month, format: :month).capitalize
  end

  def filter_summer_months
    return false if self.fee_month.month === 7 && [38,53,50].include?(family.id)
  end

  def self.due_duration
    25.days
  end

  ### DEFAULT SETTERS

  def set_invoice_date
    self.invoice_date ||= Date.today
  end

  # Defaults to 1st day of current month
  def set_fee_month
    now = Date.today
    self.fee_month ||= now.change(day: 1)
  end

  def set_due_date
    self.due_date ||= self.invoice_date + self.class.due_duration
  end

  def set_amount
    return false unless family
    self.amount ||= family.total_fee.round
    self.amount_due ||= self.amount
  end

  # More about OCR number validation on http://www.bankgirot.se/tjanster/inbetalningar/bankgiro-inbetalningar/ocr-referenskontroll/
  def set_ocr
    base = family.kids.active.map{|kid| kid.ssn[0,8].to_i }.sum
    serial = base +
      self.invoice_date.to_s.gsub(/\D/, '').to_i +
      self.amount.round +
      [family.mother_name, family.father_name].join.length
    self.ocr ||= "#{serial}#{Luhn.control_digit(serial)}"
  end

  def not_fully_payed?
    self.amount_due > 0
  end

  def fully_payed?
    !not_fully_payed?
  end

  def payed_amount # TODO: Take into account payments..?
    amount - amount_due
  end

  def overpayed?
    return false unless payments.any?
    payments.sum(:amount).round.to_i > self.amount.round.to_i
  end

  ### ACTIONS

  def pay!(money, given_on = Date.today)
    # FIXME: this will raise in Payment.create_from_bundles if an invoice is canceled
    raise 'Kan inte betala makulerad faktura' if self.canceled?
    return unless money
    # Remember that the fee amount might be different:
    if money.is_a?(Numeric)
      still_due = self.amount_due - money
    elsif money === true
      still_due = 0
    end
    update!(
      amount_due: still_due,
      payed_on: given_on
    )
  end

  def cancel!
    update!(canceled: true)
  end

  # TODO: should have different OCR?
  def replace!
    cancel!
    attrs = self.slice(
      :family_id,
      :invoice_date,
      :due_date,
      :fee_month,
      :last_reminded,
      :last_alert
    )
    replacement = Invoice.create!(attrs)
    replacement.pay!(payed_amount.nonzero?)
    # TODO: notify them?
  end

  def notice!
    FeeMailer.notice(self).deliver_now
    update!(last_reminded: Date.today)
  end

  def reminder!
    FeeMailer.reminder(self).deliver_now
    update!(last_reminded: Date.today)
  end

  def alert!
    FeeMailer.alert(self).deliver_now
    update!(last_alert: Date.today)
  end

  ### CLASS ACTIONS / JOBS

  def self.create_for_families(fee_month: nil, retroactive: true)
    now = I18n.l(Time.now, format: :short)
    month = I18n.l(fee_month || Date.today, format: :month)
    puts "INVOICE CREATION started on #{now} for #{month}..."
    Family.has_nonzero_fee.map do |family|
      begin
        print "Creating invoice for family: #{family.id} (#{family.name})"

        if fee_month
          due_date = if retroactive
            fee_month + Invoice.due_duration
          end
          invoice_date = if retroactive
            fee_month
          end
          print " with retroactive due date #{due_date.to_s}"

          family.invoices.create!(
            fee_month: fee_month,
            invoice_date: invoice_date,
            due_date: due_date
          )

        else
          family.invoices.create!
        end

        print " -> Done!"

      rescue => e
        puts "Error creating invoice: #{e}"
      ensure
        print "\n"
      end
    end
  rescue => e
    puts "Error preparing invoices to create: #{e}"
  ensure
    puts "---"
  end

  def self.send_notices_and_reminders
    self.send_notices
    self.send_reminders unless not PaymentCheck.successful_lately?
  end

  def self.for_notices
    not_reminded.where(invoice_date: Date.today - 1).valid.by_date_asc
  end

  def self.send_notices
    now = I18n.l(Time.now, format: :short)
    puts "NOTICES processing started on #{now}..."
    Invoice.for_notices.map do |invoice|
      begin
        print "Checking invoice: #{invoice.id} (#{invoice.family.name}) for #{invoice.fee_month_human}"

        invoice.notice!
        print " -> Notified!"

      rescue => e
        puts "Error while sending notice: #{e}"
      ensure
        print "\n"
      end
    end
  rescue => e
    puts "Error preparing invoices to notify about: #{e}"
  ensure
    puts "---"
  end


  def self.for_reminders
    overdue.not_recently_reminded.valid.by_date_asc
  end

  def self.send_reminders
    now = I18n.l(Time.now, format: :short)
    puts "REMINDERS processing started on #{now}..."
    Invoice.for_reminders.map do |invoice|
      begin
        print "Checking invoice: #{invoice.id} (#{invoice.family.name}) for #{invoice.fee_month_human} #{invoice.last_reminded ? '-- Last notified/reminded ' + I18n.l(invoice.last_reminded, format: :short) : ''}"

        case invoice.overdue_days
        when 7
          invoice.reminder!
          print " -> First reminder sent, 7 days overdue today!"
        when 14
          invoice.reminder!
          print " -> Second reminder sent, 14 days overdue today!"
        when 21
          invoice.alert!
          print " -> First alert triggered, 21 days overdue today!"
        when 42
          invoice.alert!
          print " -> Second alert triggered, 42 days overdue today!"
        when 84
          invoice.alert!
          print " -> Third alert triggered, 84 days overdue today!"
        end

      rescue => e
        puts "Error while sending reminder: #{e}"
      ensure
        print "\n"
      end
    end
  rescue => e
    puts "Error preparing invoices to remind about: #{e}"
  ensure
    puts "---"
  end

end
