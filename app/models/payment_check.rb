class PaymentCheck < ActiveRecord::Base
  scope :successful, -> { where(success: true) }
  scope :failed, -> { where(success: false) }

  def self.successful_lately?
    y = PaymentCheck.successful.last
    y ? y.updated_at > 9.days.ago : false
  end

end
