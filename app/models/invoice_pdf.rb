class InvoicePdf < Prawn::Document

  def initialize(invoice, view)
    @invoice = invoice
    @view = view
    super(metadata)

    load_fonts
    font "Merriweather Sans", style: :light

    move_down 72
    qr_code
    details
    message
    giro_info
    ocr_fields
  end

  def details
    lh = 3.6
    light = 0.6
    self.font_size = 12

    float do
      bounding_box([0, cursor], width: 110) do
        transparent light, light do
          pad_bottom(lh) { text 'Fakturanummer', align: :right }
          pad_bottom(lh) { text 'Kundnummer', align: :right }
          pad_bottom(lh) { text 'Familj', align: :right }
          pad_bottom(lh) { text 'Fakturadatum', align: :right }
        end
        pad_bottom(lh) { text 'Förfallodatum', align: :right, style: :bold }
        pad_bottom(lh) { text 'Gäller månad', align: :right }
        pad_bottom(lh) { text 'Summa', align: :right }
        pad_bottom(lh) { text 'Att betala', align: :right }
        pad_bottom(lh) { text 'OCR', align: :right }
      end
    end
    bounding_box([120, cursor], width: 200) do
      transparent light, light do
        pad_bottom(lh) { text @invoice.id.to_s }
        pad_bottom(lh) { text @invoice.family.id.to_s }
        pad_bottom(lh) { text @invoice.family.surnames }
        pad_bottom(lh) { text @invoice.invoice_date.to_s }
      end
      pad_bottom(lh) { text @invoice.due_date.to_s, style: :bold }
      pad_bottom(lh) { text @invoice.fee_month_human }
      pad_bottom(lh) { text "#{@invoice.amount.round.to_i} kr" }
      pad_bottom(lh) { text "#{@invoice.amount_due.round.to_i} kr" }
      pad_bottom(lh) { text @invoice.ocr }
    end
  end

  def qr_code
    sz = 110
    float do
      bounding_box([bounds.width-(sz+10), cursor+10], width: sz) do
        payment_info = JSON.dump({
          uqr: 1,
          tp: 1,
          nme: our_name,
          cid: organisation_id,
          iref: @invoice.ocr,
          ddt: @invoice.due_date.to_s.gsub(/\D/, ''),
          due: @invoice.amount_due.round.to_i,
          pt: 'BG',
          acc: giro_nr
        })
        print_qr_code(payment_info,
          extent: bounds.width,
          stroke: false
        )

        self.font_size = 9
        text_box("I bankens mobilapp kan du betala snabbt genom att scanna denna QR-kod.",
          at: [8, -8],
          # align: :right,
          width: sz-10)
      end
    end
  end

  def message
    self.font_size = 12

    msg = "Hej! Här kommer månadens faktura för er#{kids.many? ? "a":"t"} barns plats på Förskolan Solsidan.

    #{kid_fees}"
    unless @invoice.fully_payed?
      msg += "
      Var god betala in totalt #{@invoice.amount_due.round.to_i} kr till BG #{giro_nr}.

      Tack!"
    end

    move_down 60
    # text(msg)
    # stroke_bounds
    # bounding_box([cursor, cursor], pos: [-200,-200], width: 200) do
    #   text(msg)
    #   stroke_bounds
    # end
    text_box(msg,
      at: [bounds.left, cursor],
      width: 310
    )

    if @invoice.fully_payed?
      self.font_size = 24
      self.fill_color 'D0EAA1'
      text_box('DENNA FAKTURA ÄR BETALD',
        at: [bounds.right-500, 340],
        width: 500,
        align: :center,
        style: :bold,
        rotate: 6
      )
    end
    if @invoice.canceled?
      self.font_size = 24
      self.fill_color 'EEB6A4'
      text_box('DENNA FAKTURA ÄR MAKULERAD',
        at: [bounds.right-500, 340],
        width: 500,
        align: :center,
        style: :bold,
        rotate: -4
      )
    end
    self.fill_color '000000'
    self.font_size = 12
  end

  def kid_fees
    # We don't want discrepancy between total amount and kid fees
    # if we're watching an invoice from the past
    return nil unless Date.today.month === @invoice.fee_month.month
    # and similar for family with zero total_fee
    return nil unless @invoice.family.total_fee > 0

    kids.map do |kid|
      "#{kid.forename}: #{kid.fee.round.to_i} kr"
    end.join("  ")
  end

  def kids
    @invoice.family.kids.active
  end

  def giro_info
    font "Helvetica"
    self.font_size = 10

    text_box(@invoice.due_date.to_s, at: [355, 145], style: :bold)
    text_box(@invoice.ocr, at: [355, 133], style: :bold)

    text_box(giro_nr, at: [275, 72])
    text_box("Förskolan Solsidan AB", at: [350, 72])
  end

  def ocr_fields
    font "OCR B", style: :normal
    self.font_size = 11
    cs = 1.0

    text_box(@invoice.ocr,
      at: [20, 21],
      align: :right,
      character_spacing: cs,
      width: 150)

    text_box(@invoice.amount_due.round.to_i.to_s+' 00',
      at: [168, 21], # 17 mm från botten # margin_box.bottom+ # .5.in = 36 pt # 17.mm = 48 pt # ska tydligen vara 21
      align: :right,
      character_spacing: cs,
      width: 100)

    text_box(Luhn.control_digit(@invoice.amount_due.round.to_i).to_s,
      at: [194, 21],
      align: :right,
      character_spacing: cs,
      width: 100)

    text_box("#{giro_nr.gsub('-', '')} 41",
      at: [411.5, 21],
      align: :right,
      character_spacing: cs,
      width: 100)

  end

  def giro_nr
    "502-4500"
  end

  def our_name
    'Förskolan Solsidan AB'
  end

  def organisation_id
    '556932-9880'
  end

  def metadata
    {
      info: {
        Title: "Faktura #{@invoice.id} från #{our_name}",
        Author: "Mats & Fredrik Bränström, #{our_name}",
        Subject: "Förskoleplats, #{our_name}",
        Producer: "Prawn",
        CreationDate: Time.now
      },

      template: "#{Rails.root}/config/pdf/invoice_template.pdf",

      top_margin: 36,
      bottom_margin: 36,
      left_margin: 42,
      right_margin: 42
    }
  end

  def filename
    "solsidan_faktura_#{@invoice.id}.pdf"
  end

  def load_fonts
    self.font_families["OCR B"] = {
      normal: {
        file: "#{Rails.root}/app/assets/fonts/OCR/OcrB Regular.ttf",
        # file: "#{Rails.root}/app/assets/fonts/OCR/OCRBStd.otf",
        font: "OCR B"
      }
    }

    self.font_families["Merriweather Sans"] = {
      normal: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-Regular.ttf",
        font: "Merriweather Sans Regular"
      },
      italic: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-Italic.ttf",
        font: "Merriweather Sans Italic"
      },
      bold: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-Bold.ttf",
        font: "Merriweather Sans Bold"
      },
      boldItalic: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-BoldItalic.ttf",
        font: "Merriweather Sans Bold Italic"
      },
      extraBold: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-ExtraBold.ttf",
        font: "Merriweather Sans Extra Bold"
      },
      extraBoldItalic: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-ExtraBoldItalic.ttf",
        font: "Merriweather Sans Extra Bold Italic"
      },
      light: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-Light.ttf",
        font: "Merriweather Sans Light"
      },
      lightItalic: {
        file: "#{Rails.root}/app/assets/fonts/Merriweather_Sans/MerriweatherSans-LightItalic.ttf",
        font: "Merriweather Sans Light Italic"
      }
    }
  end

end