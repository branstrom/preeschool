class Payment < ActiveRecord::Base
  validates_presence_of :amount, :given_on #, :from, :reference
  validates_uniqueness_of :amount, scope: [:reference, :from, :given_on]

  belongs_to :invoice
  before_create :match_to_invoice #, on: :create
  after_create :pay_invoice #, on: :create

  normalize_attribute :reference
  normalize_attribute :amount do |amount|
    amount.is_a?(String) ? amount.gsub(/\s/, '').gsub(/,/, '.') : amount
  end

  # def from_name
  #   if self.from.index(/,\s/)
  # end

  def pay_invoice
    return true unless self.invoice &.not_fully_payed?
    invoice.pay!(self.amount, self.given_on)
    return true
  end

  def match_to_invoice
    if self.reference
      # Try matching based on OCR
      self.invoice = Invoice.valid.where(ocr: self.reference).oldest
      return true if self.invoice

      # Try matching to OCR format '141124 1609' (kid date of birth + fee month)
      # TODO: enable matching to multiple fee months, like '150213 1609+1610'
      kid = Kid.where(ssn: reference_ssn).active.first
      kid ||= Kid.active.find {|kid| kid.date_of_birth.to_s(:db).gsub(/\D/, '').slice(2,6) === reference_ssn }
      self.invoice = kid.family.invoices.valid.where(fee_month: reference_fee_month).oldest if kid
      return true if self.invoice
    end

    # Try matching based on parent name
    fam = Family.where.or(Family.where(mother_name: self.from_name), Family.where(father_name: self.from_name)).first
    self.invoice ||= fam.invoices.valid.not_fully_payed.oldest if fam
    return true
  end

  def reference_parts
    ref = self.reference.gsub(/\D/, '')
    [ref.slice(0,6), ref.slice(6,4)]
  end
  def reference_ssn
    return nil unless self.reference
    reference_parts.first
  end
  def reference_fee_month
    return nil unless self.reference
    str = reference_parts.last
    year = str[0,2].to_i+2000
    month = str[2,4].to_i
    Date.new(year,month,1)
  end

  def from_name
    return nil unless self.from
    parts = self.from.mb_chars.strip.split(/,\s?/)
    parts.reverse.join(' ').mb_chars.titleize.to_s
  end

  # TODO: don't waste any time whatsoever on already existing payments
  def self.create_from_bundles!(bundles)
    bundles.each do |bankgiro_date, payments|
      payments.each do |payment|
        payment_info = payment.merge(given_on: bankgiro_date)
        begin
          Payment.create!(payment_info)

        rescue ActiveRecord::RecordInvalid => e
          if e.message =~ /Amount måste anges/
            logger.error 'Tried to create payment without amount:'
            logger.info payment_info
            # Ignore
          elsif e.message =~ /Amount används redan/
            # Ignore validation for payment uniqueness.
            # The unique DB index could also cause ActiveRecord::RecordNotUnique,
            # which is ignored below
            logger.error 'Tried to create duplicate payment:'
            logger.info payment_info
          else
            raise e
          end

        rescue ActiveRecord::RecordNotUnique => e
          # Ignore
          logger.error 'Tried to create duplicate payment:'
          logger.info payment_info
        end
      end
    end

    Payment.where(created_at: Date.today..Date.tomorrow)
  end

  def self.try_to_fetch_new!
    check = PaymentCheck.create
    bundles = BankSpider.run
    logger.info "PAYMENT BUNDLES FROM BANKGIROT:"
    logger.info bundles
    # pp bundles
    payments = self.create_from_bundles!(bundles)
    check.update_attribute(:success, true) if payments&.any?
    payments
  end

  def self.email_and_try_to_fetch_new!
    FeeMailer.payments_fetch_alert.deliver_now
    sleep(20)
    self.try_to_fetch_new!
  end

  def overdue_days
    (self.given_on - self.invoice.due_date).to_i if self.invoice
  end

  def self.received_lately?
    y = Payment.last
    y ? y.created_at > 9.days.ago : false
  end

end
